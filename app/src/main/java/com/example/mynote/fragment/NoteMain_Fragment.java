package com.example.mynote.fragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mynote.R;
import com.example.mynote.room.DatabaseClient;
import com.example.mynote.room.NoteEntity;

import java.util.Collections;
import java.util.List;

public class NoteMain_Fragment extends Fragment {

    private RecyclerView recyclerView;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_note_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        recyclerView = view.findViewById(R.id.gallery_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        Button button = view.findViewById(R.id.newNote);
        final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navController.navigate(R.id.action_note_fragment_to_edit_fragment);
            }
        });
        getTasks();

    }


    private void getTasks() {
        class GetTasks extends AsyncTask<Void, Void, List<NoteEntity>> {

            @Override
            protected List<NoteEntity> doInBackground(Void... voids) {
                List<NoteEntity> taskList = DatabaseClient
                        .getInstance(getActivity())
                        .getAppDatabase()
                        .noteDao()
                        .getAll();
                return taskList;
            }

            @Override
            protected void onPostExecute(List<NoteEntity> tasks) {
                super.onPostExecute(tasks);
                Collections.reverse(tasks);
                TasksAdapter adapter = new TasksAdapter(getActivity(), tasks);
                recyclerView.setAdapter(adapter);
            }
        }

        GetTasks gt = new GetTasks();
        gt.execute();
    }

}
