package com.example.mynote.fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.mynote.R;

public class ShowNote extends Fragment {
    TextView notecontent , notedate;
    ImageView image_back;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_show_note, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        notecontent = view.findViewById(R.id.note_content);
        notedate = view.findViewById(R.id.note_date);
        image_back = view.findViewById(R.id.image_back);

        String note_content = getArguments().getString("note_content");
        String note_date_str = getArguments().getString("note_date_str");

        notecontent.setText(note_content);
        notedate.setText(note_date_str);

        image_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);
                navController.navigate(R.id.action_show_fragment_to_note_fragment );

            }
        });



    }
}
