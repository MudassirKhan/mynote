package com.example.mynote.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.mynote.R;
import com.example.mynote.room.NoteEntity;

import java.util.List;

public class TasksAdapter extends RecyclerView.Adapter<TasksAdapter.TasksViewHolder> {

    private Context mCtx;
    private List<NoteEntity> taskList;
    String note_content ,note_date_str;

    public TasksAdapter(Context mCtx, List<NoteEntity> taskList) {
        this.mCtx = mCtx;
        this.taskList = taskList;
    }

    @Override
    public TasksViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mCtx).inflate(R.layout.note_row, parent, false);
        return new TasksViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TasksViewHolder holder, int position) {
        NoteEntity t = taskList.get(position);
        holder.note_text.setText(t.getNote_message());
        holder.note_date.setText(t.getDate_str());

      //  note_content = t.getNote_message();
       // note_date_str = t.getDate_str();
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    class TasksViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView note_text, note_date, textViewDesc, textViewFinishBy;

        public TasksViewHolder(View itemView) {
            super(itemView);

            note_date = itemView.findViewById(R.id.note_date);
            note_text = itemView.findViewById(R.id.note_text);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            NoteEntity task = taskList.get(getAdapterPosition());

            note_content = task.getNote_message();
            note_date_str = task.getDate_str();
            Bundle args = new Bundle();
            args.putString("note_content", note_content);
            args.putString("note_date_str", note_date_str);

            final NavController navController = Navigation.findNavController((Activity) mCtx, R.id.my_nav_host_fragment);
            navController.navigate(R.id.action_note_fragment_to_ShowNote , args);

        }
    }
}
