package com.example.mynote.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;

import com.example.mynote.R;
import com.example.mynote.room.DatabaseClient;
import com.example.mynote.room.NoteEntity;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.textfield.TextInputEditText;

import java.util.Date;

public class EditNote_Fragment extends Fragment {

    TextInputEditText filledTextField;
    MaterialButton savenote , cancel;
    Activity activity;
    String currentDateTimeString;
    LinearLayout backviewll;
    ImageView image_back;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_edit_note, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        savenote = view.findViewById(R.id.savenote);
        cancel = view.findViewById(R.id.cancel);
        backviewll = view.findViewById(R.id.backviewll);
        image_back = view.findViewById(R.id.image_back);

        filledTextField = view.findViewById(R.id.filledTextField);
        activity = getActivity();

        savenote.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 currentDateTimeString = java.text.DateFormat.getDateTimeInstance().format(new Date());
              //  Toast.makeText(getContext(), currentDateTimeString, Toast.LENGTH_SHORT).show();

                saveTask();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });

        image_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().onBackPressed();
            }
        });
    }

    private void saveTask() {

        final String note_description = filledTextField.getText().toString().trim();

        if (note_description.isEmpty()) {
            Toast.makeText(getContext(), R.string.please_enter_note, Toast.LENGTH_SHORT).show();
            return;
        }

        class SaveTask extends AsyncTask<Void, Void, Void> {

            @SuppressLint("WrongThread")
            @Override
            protected Void doInBackground(Void... voids) {

                int id = 0;
                //creating a task
                NoteEntity task = new NoteEntity( id,note_description,currentDateTimeString);
                task.setNote_message(note_description);
                task.setDate_str(currentDateTimeString);

                //adding to database
                DatabaseClient.getInstance(getActivity().getApplicationContext()).getAppDatabase()
                        .noteDao()
                        .insert(task);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                Bundle args = new Bundle();

                args.putString("note_content", filledTextField.getText().toString().trim());
                args.putString("note_date_str", currentDateTimeString);

                final NavController navController = Navigation.findNavController(getActivity(), R.id.my_nav_host_fragment);
                navController.navigate(R.id.action_edit_fragment_to_ShowNote , args);

                Toast.makeText(getActivity().getApplicationContext(), "Saved", Toast.LENGTH_LONG).show();
            }
        }

        SaveTask st = new SaveTask();
        st.execute();
    }


}
