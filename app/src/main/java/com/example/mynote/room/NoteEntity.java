package com.example.mynote.room;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class NoteEntity implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int note_id;

    @ColumnInfo(name = "note_message")
    private String note_message;


    @ColumnInfo(name = "date_str")
    private String date_str;

    public NoteEntity(int note_id, String note_message, String date_str) {
        this.note_id = note_id;
        this.note_message = note_message;
        this.date_str = date_str;
    }

    public int getNote_id() {
        return note_id;
    }

    public void setNote_id(int note_id) {
        this.note_id = note_id;
    }

    public String getNote_message() {
        return note_message;
    }

    public void setNote_message(String note_message) {
        this.note_message = note_message;
    }

    public String getDate_str() {
        return date_str;
    }

    public void setDate_str(String date_str) {
        this.date_str = date_str;
    }
}

