package com.example.mynote.room;

import android.content.Context;

import androidx.room.Room;

public class DatabaseClient {

    public static DatabaseClient mInstance;
    public static NoteDatabase appDatabase;

    private DatabaseClient(Context mContext) {

        //creating the app database with Room database builder
        //MyToDos is the name of the database
        appDatabase = Room.databaseBuilder(mContext, NoteDatabase.class, "MyToDos").build();
    }

    public static synchronized DatabaseClient getInstance(Context mCtx) {
        if (mInstance == null) {
            mInstance = new DatabaseClient(mCtx);
        }
        return mInstance;
    }

    public NoteDatabase getAppDatabase() {
        return appDatabase;
    }
}
