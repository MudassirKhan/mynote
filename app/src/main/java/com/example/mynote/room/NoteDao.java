package com.example.mynote.room;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;
@Dao
public interface NoteDao {

    @Query("SELECT * FROM NoteEntity")
    List<NoteEntity> getAll();

    @Insert
    void insert(NoteEntity task);

    @Delete
    void delete(NoteEntity task);

    @Update
    void update(NoteEntity task);
}
